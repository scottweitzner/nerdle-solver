import random
import re
from functools import reduce
from typing import Dict, Iterable, List, Set

import click

VALID_EQUATION_LENGTH = 8
DIGITS = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
OPERATIONS = {"+", "-", "*", "/"}


def num_chars_unique(string) -> int:
    chars = [c for c in string]
    return len(set(chars))


def generate_all_patterns(partial_pattern=None, patterns=[]) -> List[str]:
    # account for minimal length answer and =
    if (
        partial_pattern is not None
        and len(partial_pattern) >= VALID_EQUATION_LENGTH - 2
    ):
        return

    if partial_pattern is None:
        for digit in DIGITS:
            generate_all_patterns(digit, [])
        return patterns

    if len(partial_pattern) in [4, 5, 6] and partial_pattern[-1] not in OPERATIONS:
        patterns.append(partial_pattern)

    for digit in DIGITS:
        # divide by zero
        if digit == "0" and partial_pattern[-1] == "/":
            continue
        generate_all_patterns(partial_pattern + digit)

    if partial_pattern[-1] not in OPERATIONS:
        for op in OPERATIONS:
            generate_all_patterns(partial_pattern + op)


def patterns_to_exppressions(patterns: List[str]) -> List[str]:
    expressions = set(patterns)
    for pattern in patterns:
        # remove patterns that contain numbers with leading zeros or lone zeros
        split = re.split(r"\D+", pattern)
        for num in split:
            if num.startswith("0"):
                expressions.remove(pattern)
                break

    return expressions


def form_equations(expressions: Iterable[str]):
    equations = []
    for expression in expressions:
        answer = float(eval(expression))
        if (
            answer.is_integer()
            and answer >= 0
            and len(str(int(answer))) + len(expression) == VALID_EQUATION_LENGTH - 1
        ):
            equations.append(expression + "=" + str(int(answer)))
    return equations


def find_best_first_guesses(equations: List[str]):
    def count_ops(equation: str) -> int:
        return reduce(
            lambda count, char: count + 1 if char in OPERATIONS else count + 0,
            equation,
            0,
        )

    return [
        equation
        for equation in equations
        if num_chars_unique(equation) == len(equation) and count_ops(equation) > 1
    ]


def filter_equations(
    equations, must_have: Set[str], possible_chars_for_position: Dict[int, Set[str]]
) -> List[str]:
    ret = []
    for equation in equations:
        chars = [char for char in equation]

        # check is equation has required characters
        if set(chars).intersection(must_have) != must_have:
            continue

        # check positions
        positions_mask = [
            char in possible_chars_for_position[idx] for idx, char in enumerate(chars)
        ]
        if sum(positions_mask) != VALID_EQUATION_LENGTH:
            continue

        # valid equation
        ret.append(equation)

    return ret

def valid_result_code(result_code: str) -> bool:
        if len(result_code) != VALID_EQUATION_LENGTH:
            return False
        for char in result_code:
            if char not in {"x", "o", "/"}:
                return False
        return True 

def answer_is_correct(result_code: str) -> bool:
    return result_code == "o" * VALID_EQUATION_LENGTH

def reconcile_result(guess: str, result_code: str, must_have: Set[str], possible_chars_for_position: Dict[int, Set[str]]) -> None:
    for idx, code in enumerate(result_code):
        matched_char = guess[idx]
        
        if code == "x":
            for possible_chars_for_index in possible_chars_for_position.values():
                if matched_char in possible_chars_for_index and len(possible_chars_for_index) != 1:
                    possible_chars_for_index.remove(matched_char)
        if code == "/":
            possible_chars_for_position[idx].remove(matched_char)
            must_have.add(matched_char)
        if code == "o":
            possible_chars_for_position[idx] = {matched_char}

click.command()
def nerdle():
    patterns = generate_all_patterns()
    expressions = patterns_to_exppressions(patterns)
    equations = form_equations(expressions)
    first_guesses = find_best_first_guesses(equations)

    must_have = set()
    possible_chars_for_position = {
        0: DIGITS.copy(),
        1: DIGITS.copy() | OPERATIONS.copy(),
        2: DIGITS.copy() | OPERATIONS.copy(),
        3: DIGITS.copy() | OPERATIONS.copy(),
        4: DIGITS.copy() | OPERATIONS.copy() | {"="},
        5: DIGITS.copy() | OPERATIONS.copy() | {"="},
        6: DIGITS.copy() | {"="},
        7: DIGITS.copy(),
    }

    remaining_possibilities = equations.copy()
    last_guess = random.choice(first_guesses)
    
    click.echo(click.style("🤓 Welcome to nerdle solver!", fg="blue"))
    click.echo(click.style("Instructions for recording result: ", fg="blue") + click.style("\n  x = character not in answer", fg="red") + click.style("\n  o = right character right place", fg="green") + "\n  / = right character wrong place")
    click.echo(click.style("There are ", fg="blue") + click.style(len(remaining_possibilities), fg="green") + click.style(" remaining possible answers", fg="blue"))
    click.echo(click.style("Here's a good first guess: ", fg="blue") +  click.style(last_guess, fg="green"))

    tries = 1
    while True:
        result_code = click.prompt(click.style("what happened?", fg="blue"), type=str)

        if answer_is_correct(result_code):
            click.echo(click.style(f"Congrats! You won in {tries} tries", fg="green", bold=True))
            return

        if not valid_result_code(result_code) :
            click.echo(click.style("invalid result code", fg="red"))
            continue
        
        reconcile_result(last_guess, result_code, must_have, possible_chars_for_position)
        remaining_possibilities = filter_equations(remaining_possibilities, must_have, possible_chars_for_position)

        if len(remaining_possibilities) == 0:
            click.echo(click.style("Uh oh! There are no remaining answers. You may have input the response wrong OR there may be a bug! Try again.", fg="red"))
            return

        last_guess = random.choice(remaining_possibilities)
        click.echo(click.style("There are ", fg="blue") + click.style(len(remaining_possibilities), fg="green") + click.style(" remaining possible answers", fg="blue"))
        click.echo(click.style("Try this: ", fg="blue") + click.style(last_guess, fg="green"))

        tries += 1
