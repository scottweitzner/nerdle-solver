# 🤓 Nerdle Solver
### Huge props to the team at [Nerdle](https://nerdlegame.com/) for building this wordle-inspired game

I built this because I found the properties of the answers to this game very intersting. What started with me scribbling out possible patterns and playing with different start-word heuristics eventually led me to create this.

According to the logic in my code there are **11833** possible answers to this game. (Take this with a grain of salt in case I have some bugs)

## How to play:
nerdle-solver will feed you answers you just have to record the response code in a single string as such:

```text
  x = character not in answer
  o = right character right place
  / = right character wrong place
```

## How to run

```shell
poetry install
poetry shell
poetry run nerdle
```